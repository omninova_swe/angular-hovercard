angular-hovercard
---

### Installing

With Bower from git-repo (you will be asked for Bitbucket login credentials)  
```
bower install [--save] https://bitbucket.org/omninova_swe/angular-hovercard.git
```   

With Bower from precloned into local path (not recommanded for production)  
```
bower install [path to local folder]
```   

Script-include in your html.  
```
<script src="bower_components/omninova-angular-hovercard/dist/angular-hovercard.min.js"></script>
``` 
 
and optionally
```
<link rel="stylesheet" href="dist/angular-hovercard.min.css">
``` 

then  
```
angular.module('myApp', ['omninova.ngHoverCard']);
```

For changes (in `index.js`) this library must be rebuilt because of npm dependencies (se below)

#### Dependencies
This module is dependent on JQuery and Angular (in that order). CDN and/or local install.  
```
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
<script src="bower_components/angular/angular.min.js"></script>
```  
Also this module has `hoverintent` built in so it _should not_ be loaded manually. Usage can be found at:  
[https://github.com/tristen/hoverintent](https://github.com/tristen/hoverintent)


### Usage
```
<hover-card>Lorem ipsum</hover-card>
```
or
```
<div hover-card hover-card-template="mytemplate.html">Dolor sit amet</div>
``` 
See demo.  
Style can be controlled from css  
z-index defaults to 1 (if not specified) 
```
hover-card-dialog {
    background-color: azure;
    font-weight: bold;
    font-size: large;
    width: 300px;
    height: 150px;
    z-index: 10;
}
```
Note that some css is built in, like `position:fixed`, positioning and default dimensions.  
Also note that the css selector for the actual hovercard is `hover-card-dialog` (and not _hover-card_)

#### Attributes
Directive can have following attributes
- template : The html-template the hovercard should use
- callbackIn : A callback executing when hovercard shows (hover in)
- callbackOut : A callback executing when hovercard closes (hover out)
```
<div hover-card hover-card-callback-in="fillContent(s.name)" hover-card-callback-out="emptyContent()"   
hover-card-template="demo_template_saga.html">
    Hover here to trigger a hovercard
</div>
```

### Update
To get new changes from source (repo or local folder). You may be asked again for credentials.  
```
bower update omninova-angular-hovercard
```   
Force flag is necessary since we do not bother using versioning.

### Demo
For just local viewing of demo no dependencies are needed (angular is used through cdn).

### Developing
Changes are made in src/index.js and must be compiled using `npm debug` or `npm build` into dist/angular-hovercard.js. Se below.  
Note that the src/index.js can not be used directly.  
Bower version (and git tag) should be updated in new releases.  
Demo files are just to demo/showcase the use of the library.

### Building

to manage dependencies and build. Development requires you
have [node.js](http://nodejs.org) installed.

1. [Install node.js](http://nodejs.org/). 'Install' will download a package for
your OS.
3. Run `npm install`
4. Run `npm run build` or `npm run debug`


### Running demo
As with all SPA (and some plain Angular too) files must be runned through a server for api-calls not to get caught by CORS.  
For instance can the library `http-server` can be used for this (installs global).  
[http://stackoverflow.com/a/16350826/2750774](http://stackoverflow.com/a/16350826/2750774)

#### NPM scripts
The predefined npm-scripts are dependent on installs of some libraries:  
 - debug/build: Used to "compile" (assamble and minify) script from src/ to dist/
 - watch: Watch filechanges and execute script (e.g. debug/build)
 - livereload-server: `npm install -g livereload-server` - Watch filechanges and trigger browser refresh
 - server: `npm install -g http-server` - Run server in current folder