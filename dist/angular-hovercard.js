(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.angularHovercard = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
;(function(global) {
'use strict';

var extend = require('xtend');

var hoverintent = function(el, onOver, onOut) {
  var x, y, pX, pY;
  var h = {},
    state = 0,
    timer = 0;

  var options = {
    sensitivity: 7,
    interval: 100,
    timeout: 0
  };

  function delay(el, e) {
    if (timer) timer = clearTimeout(timer);
    state = 0;
    return onOut.call(el, e);
  }

  function tracker(e) {
    x = e.clientX;
    y = e.clientY;
  }

  function compare(el, e) {
    if (timer) timer = clearTimeout(timer);
    if ((Math.abs(pX - x) + Math.abs(pY - y)) < options.sensitivity) {
      state = 1;
      return onOver.call(el, e);
    } else {
      pX = x;
      pY = y;
      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }
  }

  // Public methods
  h.options = function(opt) {
    options = extend({}, options, opt);
    return h;
  };

  function dispatchOver(e) {
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state !== 1) {
      pX = e.clientX;
      pY = e.clientY;

      el.addEventListener('mousemove', tracker, false);

      timer = setTimeout(function() {
        compare(el, e);
      }, options.interval);
    }

    return this;
  }

  function dispatchOut(e) {
    if (timer) timer = clearTimeout(timer);
    el.removeEventListener('mousemove', tracker, false);

    if (state === 1) {
      timer = setTimeout(function() {
        delay(el, e);
      }, options.timeout);
    }

    return this;
  }

  h.remove = function() {
    if (!el) return;
    el.removeEventListener('mouseover', dispatchOver, false);
    el.removeEventListener('mouseout', dispatchOut, false);
  };

  if (el) {
    el.addEventListener('mouseover', dispatchOver, false);
    el.addEventListener('mouseout', dispatchOut, false);
  }

  return h;
};

global.hoverintent = hoverintent;
if (typeof module !== 'undefined' && module.exports) module.exports = hoverintent;
})(this);

},{"xtend":2}],2:[function(require,module,exports){
module.exports = extend

var hasOwnProperty = Object.prototype.hasOwnProperty;

function extend() {
    var target = {}

    for (var i = 0; i < arguments.length; i++) {
        var source = arguments[i]

        for (var key in source) {
            if (hasOwnProperty.call(source, key)) {
                target[key] = source[key]
            }
        }
    }

    return target
}

},{}],3:[function(require,module,exports){
/**
 * Created by Hakan on 2017-02-08.
 */

var hoverintent = require('hoverintent');

/**
 * Directive to wrap Hover Intent
 * http://tristen.ca/hoverintent/
 * https://github.com/tristen/hoverintent
 * AND
 * fire up a double card-popup
 */
angular.module('omninova.ngHoverCard', [])

// Put default template in template-cache
    .run(['$templateCache', function ($templateCache) {
        $templateCache.put('hover-card-popup-default', 'No template specified.');
    }])

    // The trigger directive (using hover intent)
    .directive('hoverCard', ['$compile', '$templateCache', '$window', function ($compile, $templateCache, $window) {
        return {
            restrict: 'EA',
            // directive SHOULD NOT HAVE an isolated scope
            scope: false,
            link: function (scope, element, attributes) {

                var hoverCardDialog;

                // Use content as template, if no template explicitly defined
                if (!attributes.hoverCardTemplate) {    //&& element[0].tagName === 'HOVER-CARD') {

                    var templateName = 'hover-card-popup-' + Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);

                    $templateCache.put(templateName, element.html());
                    attributes.hoverCardTemplate = templateName;
                }

                // ==========

                // Triggered by hoverIntent (set class)
                element.hoverListener = hoverintent(element[0],
                    openDialog,
                    closeDialog
                ).options({timeout: 200});

                // ==========

                function closeDialog() {
                    // remove instance-dialog from DOM
                    hoverCardDialog && hoverCardDialog.remove();

                    // Execute callback if it is present
                    if (attributes.hoverCardCallbackOut) {
                        scope.$eval(attributes.hoverCardCallbackOut)
                    }
                }

                function openDialog() {

                    // Execute callback if it is present
                    if (attributes.hoverCardCallbackIn) {
                        scope.$eval(attributes.hoverCardCallbackIn)
                    }

                    // ==========

                    // Prevent event from bubbling up, the imaginary way ("best" solution but still has side effects)
                    if (scope.preventHoverBubble)
                        return;

                    // artificial event/lock...
                    scope.preventHoverBubble = true;

                    // ==========

                    hoverCardDialog = angular.element(document.createElement('hover-card-dialog'));
                    hoverCardDialog.attr('template', attributes.hoverCardTemplate);

                    var el = $compile(hoverCardDialog)(scope);

                    // Append to element
                    element.append(el);

                    // Apply necessary css
                    // hide until position calc is done, or else flicker
                    el.css({
                        'position': 'fixed',
                        'visibility': 'hidden'
                    });
                }
            }
        };
    }])

    // The actually hoverCard-directive (dynamically added)
    .directive('hoverCardDialog', ['$window', '$timeout', function ($window, $timeout) {
        return {
            restrict: 'E',
            transclude: true,
            // directive SHOULD NOT HAVE an isolated scope
            scope: false,
            templateUrl: function (tElement, tAttrs) {

                return tAttrs.template || 'hover-card-popup-default';

            },
            link: function (scope, element, attrs) {

                /*
                 * Calculate position of hoverCard
                 * @jon
                 */

                // wait for angular to resolve (??)
                $timeout(function () {
                    position();
                }, 50);

                function position() {

                    // Find parent-hovercard, recursively
                    function findPH(element, searchName) {
                        if (!element[0].tagName) {
                            return null;
                        } else if (element[0].tagName === searchName.toUpperCase() || element[0].hasAttribute(searchName)) {
                            return element;
                        } else {
                            return findPH(element.parent(), searchName);
                        }
                    }

                    // ==========

                    // Current hovercard
                    var hover_card = element;

                    // The trigger link for the hovercard
                    var trigger_link = element.parent();

                    // If hovercard has another hovercard as parent/ancestor, it is a subhovercard
                    var parent_hover_card = findPH(trigger_link.parent(), 'hover-card');

                    // Common variables for calculations
                    var window_width = $window.innerWidth,
                        window_height = $window.innerHeight,
                        link_width = trigger_link[0].offsetWidth,
                        link_height = trigger_link[0].offsetHeight,
                        card_width = hover_card[0].offsetWidth,
                        card_height = hover_card[0].offsetHeight,
                        link_position_vertical = trigger_link[0].getBoundingClientRect().top,
                        link_position_horizontal = trigger_link[0].getBoundingClientRect().left,

                        center_vertical = (window_height - card_height) / 2,
                        // Overflow how much the card overflows to the left relative to link width (negative if not overflowing)
                        card_horizontal_overflow = (card_width - link_width  ) / 2,
                        card_vertical_overflow = (card_height - link_height ) / 2,

                        available_space_above = link_position_vertical,
                        available_space_below = window_height - (available_space_above + link_height),
                        available_space_left = link_position_horizontal,
                        available_space_right = window_width - ( available_space_left + link_width ),

                        sub_hover_card_offset = 16;


                    // obsolete
                    // Left/right margin is the space needed to put card centered relative to link
                    // var left_margin = link_position_horizontal - card_horizontal_overflow,
                    //     right_margin = left_margin + card_width;

                    /**
                     * Position hovercard if it is first level
                     */

                    // no parent means first level
                    if (!parent_hover_card) {

                        // Below are four steps to position the card where there is space.
                        // Default priority is to place card above. If thats not possible we try below, then to the right, then to the left.
                        // If none prevailed ... ???
                        // First step ------------------------------------------------------- Is there space to put card above link?
                        if (available_space_above > card_height) {
                            // console.log('first step');

                            // Top is always top
                            element.css({
                                "top": available_space_above - card_height + 'px'
                            });

                            // Place it centered
                            if (available_space_left > card_horizontal_overflow && available_space_right > card_horizontal_overflow) {
                                // console.log('centered');
                                element.css({
                                    "left": link_position_horizontal - card_horizontal_overflow + 'px'
                                });
                            } else {
                                if (link_position_horizontal - card_horizontal_overflow * 2 > 0) {
                                    // Place it to the left
                                    // console.log('left');
                                    element.css({
                                        "left": link_position_horizontal - card_horizontal_overflow * 2 + 'px'
                                    });
                                }
                                // Place it to the right
                                // else {
                                //     console.log('right');
                                //     element.css({
                                //         "left": link_position_horizontal + 'px'
                                //     });
                                // }
                            }
                            element.addClass("over");
                            element.removeClass("left");
                            element.removeClass("right");
                            element.removeClass("under");
                        }
                        // Second step ------------------------------------------------------- Is there space to put card belov link?
                        else if (available_space_below > card_height) {
                            // console.log('second step');

                            // Top is always top
                            element.css({
                                "top": available_space_above + link_height + 'px'
                            });

                            // Place it centered
                            if (available_space_left > card_horizontal_overflow && available_space_right > card_horizontal_overflow) {
                                // console.log('centered');
                                element.css({
                                    "left": link_position_horizontal - card_horizontal_overflow + 'px'
                                });
                            } else {
                                if (link_position_horizontal - card_horizontal_overflow * 2 > 0) {
                                    // Place it to the left
                                    // console.log('left');
                                    element.css({
                                        "left": link_position_horizontal - card_horizontal_overflow * 2 + 'px'
                                    });
                                }
                                // Place it to the right
                                else {
                                    // console.log('right');
                                    element.css({
                                        "left": link_position_horizontal + 'px'
                                    });
                                }
                            }

                            element.addClass("under");
                            element.removeClass("left");
                            element.removeClass("right");
                            element.removeClass("over");
                        }
                        // Third step ------------------------------------------------------- Is there space to put card to the right side of link?
                        else if (available_space_right > card_width) {
                            // console.log('third step');

                            element.css({
                                "top": center_vertical + 'px',
                                "left": link_position_horizontal + link_width + 'px'
                            });

                            element.addClass("left");
                            element.removeClass("over");
                            element.removeClass("under");
                            element.removeClass("right");
                        }
                        // Fourth step ------------------------------------------------------- We asume there is space on the left side of the link
                        else {
                            // console.log('fourth step');

                            element.css({
                                "top": center_vertical + 'px',
                                "left": link_position_horizontal - card_width + 'px'
                            });

                            element.addClass("right");
                            element.removeClass("left");
                            element.removeClass("over");
                            element.removeClass("under");
                        }

                    } else {

                        /**
                         * Position hovercard if it is upper level (subhovercard)
                         */

                        var fits = {};

                        // Here we check each case and set true if so
                        // Set flags to true where card would fit
                        if (available_space_above > card_height) {
                            // console.log("Card fits above");
                            if (( available_space_right - card_horizontal_overflow > 0 ) && ( available_space_left - card_horizontal_overflow > 0)) {
                                // console.log("Card fits above center");
                                fits.above = true;
                            }
                            if (available_space_left > card_width) {
                                // console.log("Card fits above to the left");
                                fits.above_left = true;
                            }
                            if (available_space_right > card_width) {
                                // console.log("Card fits above to the right");
                                fits.above_right = true;
                            }
                        }
                        if (available_space_below > card_height) {

                            // console.log('Card fit below');
                            if (( available_space_right - card_horizontal_overflow > 0 ) && ( available_space_left - card_horizontal_overflow > 0 )) {
                                // console.log("Card fits below center");
                                fits.below = true;
                            }
                            if (available_space_left > card_width) {
                                // console.log("Card fits below to the left");
                                fits.below_left = true;
                            }
                            if (available_space_right > card_width) {
                                // console.log("Card fits below to the right");
                                fits.below_right = true;
                            }
                        }
                        if (available_space_left > card_width) {

                            // console.log('Card fits to the left');
                            if (( available_space_below - card_vertical_overflow > 0 ) && ( available_space_left - card_vertical_overflow > 0 )) {
                                // console.log("Card fits left centered");
                                fits.left = true;
                            }
                            if (available_space_above > card_height) {
                                // console.log('Card fits left above');
                                fits.left_above = true;
                            }
                            if (available_space_below > card_height) {
                                // console.log('Card fits left below');
                                fits.left_below = true;
                            }
                        }
                        if (available_space_right > card_width) {

                            // console.log('Card fits to the right');
                            if ((available_space_below - card_vertical_overflow > 0) && ( available_space_above - card_vertical_overflow > 0)) {
                                // console.log("Card fits right centered");
                                fits.right = true;
                            }
                            if (available_space_above > card_height) {
                                // console.log('Card fits left above');
                                fits.right_above = true;
                            }
                            if (available_space_below > card_height) {
                                // console.log('Card fits left below');
                                fits.right_below = true;
                            }
                        }

                        // Variables for positioning the card vertically and horizontally. Used for simulation and final placement
                        var vertical_top = ( available_space_above - card_height + link_height ),
                            vertical_center = ( (window_height - card_height) / 2 ),
                            vertical_bottom = ( available_space_above ),

                            horizontal_left = ( link_position_horizontal - card_width ),
                            horizontal_center = ( available_space_left - card_horizontal_overflow),
                            horizontal_right = ( link_position_horizontal + link_width );

                        var hover_card_left = parent_hover_card[0].getBoundingClientRect().left,
                            hover_card_top = parent_hover_card[0].getBoundingClientRect().top,
                            hover_card_width = parent_hover_card[0].offsetWidth,
                            hover_card_height = parent_hover_card[0].offsetHeight;

                        var sub_hover_card_width = hover_card[0].offsetWidth,
                            sub_hover_card_height = hover_card[0].offsetHeight;

                        // ==========

                        function overlapArea(sub_hover_card_top, sub_hover_card_left) {
                            var width = (Math.min(hover_card_left + hover_card_width, sub_hover_card_left + sub_hover_card_width) - Math.max(hover_card_left, sub_hover_card_left)),
                                height = (Math.min(hover_card_top + hover_card_height, sub_hover_card_top + sub_hover_card_height) - Math.max(hover_card_top, sub_hover_card_top));

                            return Math.abs(width * height);
                        }

                        var areasArr = {};

                        // Set the variables. Cases where the card would not fit is not calculated. Instead we set a very high value for the cases as a default asuming that all performed calculations will show a lower value.
                        if (fits.above) {
                            areasArr.case_above_center = overlapArea(vertical_top, horizontal_center);
                        }

                        if (fits.above_left) {
                            areasArr.case_above_left = overlapArea(vertical_top, horizontal_left);
                        }

                        if (fits.above_right) {
                            areasArr.case_above_right = overlapArea(vertical_top, horizontal_right);
                        }

                        if (fits.below) {
                            areasArr.case_below_center = overlapArea(vertical_bottom, horizontal_center);
                        }

                        if (fits.below_left) {
                            areasArr.case_below_left = overlapArea(vertical_bottom, horizontal_left);
                        }

                        if (fits.below_right) {
                            areasArr.case_below_right = overlapArea(vertical_bottom, horizontal_right);
                        }

                        if (fits.left) {
                            if (( fits.above_left === false ) && ( fits.below_left === false )) {
                                areasArr.case_left_center = overlapArea(vertical_center, horizontal_left);
                            }
                        }

                        if (fits.right) {
                            if (( fits.above_right === false ) && ( fits.below_right === false )) {
                                areasArr.case_right_center = overlapArea(vertical_center, horizontal_right);
                            }
                        }

                        // Here we compare all cases to find the one with the smallest area. The smallest area is set as a variable
                        // Find the area that would give the best position
                        var best_area = Math.min.apply(Math, Object.keys(areasArr).map(function (key) {
                            return areasArr[key];
                        }));

                        // There is a possibility that many cases match the smallest area / best position, and thats why we runs if-else in the order we prefere card to be shown.
                        // A match positions the card on the screen and sets correct classes
                        if (areasArr.case_above_center === best_area) {
                            element.css({
                                "top": vertical_top - link_height + 'px',
                                "left": horizontal_center + 'px'
                            }).addClass("over").removeClass("left right under");
                        }
                        else if (areasArr.case_above_left === best_area) {
                            element.css({
                                "top": vertical_top - sub_hover_card_offset + 'px',
                                "left": horizontal_left + 'px'
                            }).addClass("over").removeClass("left right under");
                        }
                        else if (areasArr.case_above_right === best_area) {
                            element.css({
                                "top": vertical_top - sub_hover_card_offset + 'px',
                                "left": horizontal_right + 'px'
                            }).addClass("over").removeClass("left right under");
                        }
                        else if (areasArr.case_below_center === best_area) {
                            element.css({
                                "top": vertical_bottom + link_height + 'px',
                                "left": horizontal_center + 'px'
                            }).addClass("under").removeClass("left right over");
                        }
                        else if (areasArr.case_below_left === best_area) {
                            element.css({
                                "top": vertical_bottom + sub_hover_card_offset + 'px',
                                "left": horizontal_left + 'px'
                            }).addClass("under").removeClass("left right over");
                        }
                        else if (areasArr.case_below_right === best_area) {
                            element.css({
                                "top": vertical_bottom + sub_hover_card_offset + 'px',
                                "left": horizontal_right + 'px'
                            }).addClass("under").removeClass("left right over");
                        }
                        else if (areasArr.case_left_center === best_area) {
                            element.css({
                                "top": vertical_center + 'px',
                                "left": horizontal_left + 'px'
                            }).addClass("left").removeClass("under right over");
                        }
                        else if (areasArr.case_right_center === best_area) {
                            element.css({
                                "top": vertical_center + 'px',
                                "left": horizontal_right + 'px'
                            }).addClass("right").removeClass("under left over");
                        }

                    }

                    scope.preventHoverBubble = false;

                    // ==========

                    // Get inherited z-index, or default to 1
                    var zIndex = Number.parseInt($window.getComputedStyle(element[0]).getPropertyValue('z-index')) || 1;

                    // Show it on the new position
                    element.css({
                        'z-index': zIndex,
                        'visibility': 'visible'
                    });
                }


            },
            controller: ['$scope', function ($scope) {
                // Add generic scope-stuff here if needed?
            }]
        }
    }])

;
},{"hoverintent":1}]},{},[3])(3)
});